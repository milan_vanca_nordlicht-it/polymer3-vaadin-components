# polymer3-vaadin-components

Download all dependencies
npm install

Serve works as expected
./node_modules/polymer-cli/bin/polymer.js serve --open


Build doesn't work properly
./node_modules/polymer-cli/bin/polymer.js build

Because this file is missing
build/default/node_modules/@webcomponents/webcomponentsjs/bundles/webcomponents-sd-ce.js

To fix it run these commands
mkdir build/default/node_modules/@webcomponents/webcomponentsjs/bundles
cp fix/webcomponents-sd-ce.js build/default/node_modules/@webcomponents/webcomponentsjs/bundles/

Thank you for your time :)